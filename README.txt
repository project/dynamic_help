To implement Dynamic Help in your modules, call dynamic_help() exactly where you want the help to appear. Example of such a call:
<?php
$output = dynamic_help(array(
  array('message' => 'Set up permissions', 'executed' => FALSE),
  array('message' => 'Create blocks', 'executed' => TRUE)),
  'Example Module'
);
?>